import { NodeDependenciesProvider } from './tree-data-provider';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
const clickup_api = require("clickup_api");


// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export async function activate(context: vscode.ExtensionContext) {
	const apiKey = "pk_3055456_S00VC5OINGPEJ06IB5TPSWUY81FU7OG9";

	const Clickup = new clickup_api(apiKey);

	var data = {
		"name": "New Task Name",
		"content": "New Task Content",
		"status": "Open"
	};
	var info = await Clickup.Teams.get_teams();

	console.log(info);

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "clickappvscode" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('clickappvscode.helloWorld', () => {
		// The code you place here will be executed every time your command is executed
		// Display a message box to the user
		vscode.window.showInformationMessage('Hello World from ClickAppVScode!');
	});
	// {id: '3029664', name: 'CRIPTOANALISIS', color: '#f42c2c', avatar: null, members: Array(12)}
	// {id: '3047677', name: 'Monkey Workspace', color: '#0ab4ff', avatar: null, members: Array(26)}
	// {id: '3033771', name: 'Blockchain's Workspace', color: '#ffa12f', avatar: null, members: Array(0)}
	
	context.subscriptions.push(disposable);
	vscode.window.registerTreeDataProvider(
		'nodeDependencies',
		new NodeDependenciesProvider(vscode.workspace.rootPath ?? '')
	);
	
	vscode.window.createTreeView('nodeDependencies', {
		treeDataProvider: new NodeDependenciesProvider(vscode.workspace.rootPath ?? '')
	});
	const nodeDependenciesProvider = new NodeDependenciesProvider(vscode.workspace.rootPath ?? '');

  vscode.window.registerTreeDataProvider('nodeDependencies', nodeDependenciesProvider);
  vscode.commands.registerCommand('nodeDependencies.refreshEntry', () =>
    nodeDependenciesProvider.refresh()
  );

	let statusBar = vscode.commands.registerCommand('clickappvscode.status', () => {
		// The code you place here will be executed every time your command is executed
		// Display a message box to the user
		vscode.window.showInformationMessage('status from ClickAppVScode!');
	});

	const myStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 10);
	myStatusBarItem.command = 'clickappvscode.status';
	myStatusBarItem.name = "clickup";
	myStatusBarItem.text = '${record} clickup status';
	myStatusBarItem.tooltip = 'review status';
	myStatusBarItem.show();
	
}

// this method is called when your extension is deactivated
export function deactivate() { }
